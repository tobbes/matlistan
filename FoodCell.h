//
//  FoodCell.h
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-13.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
// Represents a cell in the list of all foods

#import <UIKit/UIKit.h>
#import "TE_Buy.h"
@interface FoodCell : UITableViewCell
@property (nonatomic) TE_Buy * buy;
@property (weak, nonatomic) IBOutlet UILabel *amountlabel;

@property (weak, nonatomic) IBOutlet UIButton *plusbutton;
@property (weak, nonatomic) IBOutlet UIButton *minusbutton;

@end

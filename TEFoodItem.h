//
//  TEFoodItem.h
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-01.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
// An item on the foodlist 

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>

@class TEFoodItem;

@interface TEFoodItem : NSManagedObject


@property (nonatomic, retain) UIImage * image;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic) NSString * key;
@property (nonatomic, retain) NSSet * occurences;

@end

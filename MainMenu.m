//
//  MainMenu.m
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-01.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
// The first menu which is presented to the user

#import "MainMenu.h"
#import "TEFoodItem.h"
#import "TECategory.h"
#import "DataManager.h"
#import "AllFoodTable.h"


@interface MainMenu ()

@property (nonatomic) NSString * listname;



@end



@implementation MainMenu
@synthesize listname;

-(void) viewDidLoad {
    
    }

// create a new shoppinglist */
- (IBAction)newList:(UIButton *)sender {
   
    //setup and show a dialogue to let the user choose a name for the new list
    UIAlertController * ac = [UIAlertController alertControllerWithTitle:@"Ny lista" message:@"Välj ett namn för den nya listan" preferredStyle:UIAlertControllerStyleAlert];

    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"Avbryt" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
    
        [self.navigationController popViewControllerAnimated:YES];
    
    }];
    
    //Add buttons with action to this dialogue
    UIAlertAction * confirm = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * a) {
    
        // Add a textfield to type the list name in.
        UITextField * tf = ac.textFields[0];
        
        //show it
        self.listname = tf.text;
        [self performSegueWithIdentifier:@"newlist" sender:self];
    }];
    
    
    [ac addAction:confirm];
    [ac addAction:cancel];
    
    
    [ac addTextFieldWithConfigurationHandler:^(UITextField * t) {
    
        t.backgroundColor = [UIColor redColor];
        t.text = @"";
    }];
    [self presentViewController:ac animated:YES completion:nil];

    
    }



-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    //when the new list will be created provide
    // the viewcontroller responsible for populating the list with the name of the list
    if([segue.identifier isEqualToString:@"newlist"]) {
        AllFoodTable * table =  (AllFoodTable *)segue.destinationViewController;
        table.listname = self.listname;
    }
    
}




@end

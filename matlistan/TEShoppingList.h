//
//  TEShoppingList.h
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-08.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
// This class models a shoppinglist

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TE_Buy;

@interface TEShoppingList : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSSet *bought;
@property (nonatomic, retain) NSString * name;
@end

@interface TEShoppingList (CoreDataGeneratedAccessors)

- (void)addBoughtObject:(TE_Buy *)value;
- (void)removeBoughtObject:(TE_Buy *)value;
- (void)addBought:(NSSet *)values;
- (void)removeBought:(NSSet *)values;

@end

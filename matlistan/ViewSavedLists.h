//
//  ViewSavedLists.h
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-21.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
// This viewcontroller is responsible for showing all saved shoppinglists

#import <UIKit/UIKit.h>

@interface ViewSavedLists : UITableViewController

@end

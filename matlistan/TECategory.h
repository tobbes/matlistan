//
//  TECategory.h
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-03.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TECategory : NSManagedObject

@property (nonatomic, retain) NSString * category;

@end

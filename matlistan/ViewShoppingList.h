//
//  ViewShoppingList.h
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-08.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
// Shows a shoppinglist to let the user shop from it 

#import <UIKit/UIKit.h>
#import "TEShoppingList.h"

@interface ViewShoppingList : UITableViewController
@property (nonatomic) TEShoppingList * list;
@end

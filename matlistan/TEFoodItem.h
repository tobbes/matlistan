//
//  TEFoodItem.h
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-01.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>
#import "TECategory.h"

@interface TEFoodItem : NSManagedObject

@property (nonatomic) NSString * name;
@property (nonatomic) float price;
@property (nonatomic) UIImage * image;
@property (nonatomic) TECategory * category;
@end

//
//  TEShoppingList.m
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-08.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "TEShoppingList.h"
#import "TE_Buy.h"


@implementation TEShoppingList

@dynamic id;
@dynamic bought;
@dynamic name;

@end

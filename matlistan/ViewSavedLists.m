//
//  ViewSavedLists.m
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-21.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "ViewSavedLists.h"
#import "DataManager.h"
#import "TE_Buy.h"
#import "TEShoppingList.h"
#import "ViewShoppingList.h"
@interface ViewSavedLists ()

@property (nonatomic) NSArray * savedLists;
@property (nonatomic) DataManager *  manager;
@end

@implementation ViewSavedLists

- (void)viewDidLoad {
    [super viewDidLoad];
    self.manager = [DataManager getInstance];
    self.savedLists = [self.manager getShoppingLists];
    
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.savedLists.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"thisList" forIndexPath:indexPath];
    TEShoppingList * list = self.savedLists[indexPath.row];
    NSLog(@"%@",list.name);
    cell.textLabel.text = list.name;
    return cell;
}



-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    //Set up the viewcontroller to show a specific list
    // provide it with the list to show
    if([segue.identifier isEqualToString:@"showList"] ) {
        //get the correct list and set it in the presentationviewcontroller
        ViewShoppingList * dest = (ViewShoppingList *) segue.destinationViewController;
        UITableViewCell * cell = (UITableViewCell *) sender;
        NSIndexPath * path = [self.tableView indexPathForCell:cell];
        TEShoppingList * list = self.savedLists[path.row];
        dest.list = list;
    }
}




// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        TEShoppingList * list = self.savedLists[indexPath.row];
        [self.manager deleteShoppingList:list];
        self.savedLists = [self.manager getShoppingLists];
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

-(BOOL) tableView:(UITableView *) tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"Ta bort lista";
}

@end

//
//  ViewShoppingList.m
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-08.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
// This view is used while shopping from a given list
//
#import "ViewShoppingList.h"
#import "DataManager.h"
@interface ViewShoppingList ()

@property (nonatomic) NSMutableArray * shoppingitems;
@property (nonatomic) DataManager * manager;
@property (nonatomic) UILabel * costlabel;
@property (nonatomic) double totalprice;

@end

@implementation ViewShoppingList

- (void)viewDidLoad {
    [super viewDidLoad];
    self.manager = [DataManager getInstance];
    [self refreshMe];
    CGRect r = CGRectMake(0, 0, self.tableView.frame.size.width, 10.0);
    self.costlabel = [[UILabel alloc] initWithFrame:r];
    self.costlabel.backgroundColor = [UIColor blueColor];
    self.costlabel.textColor = [UIColor redColor];
    self.costlabel.text = @"Total kostnad: ";
    }

/* Update the total cost */
-(void) updateCostWithBuy: (TE_Buy *) buy {
    NSNumber * cost  = buy.grocery.price;
    NSNumber  * antal = buy.amount;
    self.totalprice = self.totalprice + (antal.integerValue * cost.floatValue);
    self.costlabel.text = [NSString stringWithFormat:@"Total kostnad: %.2f kr",self.totalprice];
}


/* Funktionality to remove an item from the list when swiped and update the cost accordingly */
- (IBAction)didSwipe:(UISwipeGestureRecognizer *)sender {
    CGPoint pos = [sender locationInView:self.view];
    NSIndexPath *path = [self.tableView indexPathForRowAtPoint:pos];
    
    //if the list has been swiped
    if(path) {
        
        // get cell and start setting up animation to remove the cell
        [UIView setAnimationDuration:3.0];
        UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:path];
        
        // get the appropriate item from the backend, the list
        TE_Buy * buy = self.shoppingitems[path.row];
        
        cell.textLabel.text = nil;
        
        //set up a string which can be striked through
        //containing the foodproduct's name
        NSMutableAttributedString * mstring = [[NSMutableAttributedString alloc] initWithString:buy.grocery.name];
        [mstring addAttribute:NSStrikethroughStyleAttributeName value:@3 range:NSMakeRange(0, buy.grocery.name.length)];
        
        //now strike through with red and then animate the cell to the left, then remove the cell
        // and update the cost
        NSRange range = NSMakeRange(0, buy.grocery.name.length);
        [mstring addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range: range];
        cell.textLabel.attributedText = mstring;
        [self.shoppingitems removeObject:self.shoppingitems[path.row]];
        [self.tableView deleteRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationLeft];
        
        [self updateCostWithBuy:buy];
        
        }
  
}




//reloads data from the backend-list */
-(void) refreshMe {
    
    self.shoppingitems = [[self.list.bought allObjects] mutableCopy];
    [self.tableView reloadData];
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.shoppingitems.count;
    
}


/* Adds a label for the totalcost to the table footer */
-(UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return self.costlabel;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellidentifier = @"buyCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier forIndexPath:indexPath];
    TE_Buy * buy = self.shoppingitems[indexPath.row];
    NSString * antal = [NSString stringWithFormat:@" %@",buy.amount];
    cell.textLabel.text = [buy.grocery.name stringByAppendingString:antal];
    
    if(buy.grocery.image) {
        cell.imageView.image = buy.grocery.image;
    }
    
    
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

/* Adds a title for the list in the view */
-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(10 , 5, tableView.frame.size.width,10)];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor blueColor];
    label.text = self.list.name;
    return label;
}



@end

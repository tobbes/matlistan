//
//  AddFoodItemController.m
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-07.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "AddFoodItemController.h"
#import "DataManager.h"
@interface AddFoodItemController ()

@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIButton *removeButton;
@property (weak, nonatomic) IBOutlet UITextField *productText;
@property (nonatomic) DataManager * manager;
@property (nonatomic) UIImagePickerController * picController;
@property (nonatomic) UIImage * image;
@property (weak, nonatomic) IBOutlet UIImageView *imageview;

@property (weak, nonatomic) IBOutlet UITextField *priceField;

@end

@implementation AddFoodItemController

@synthesize picController = _picController;

/* Let the user take a picture of the food to be added */
- (IBAction)takePicture:(UIButton *)sender {

    // check availability of camera
    if([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        self.picController.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else {
        self.picController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    [self presentViewController:self.picController animated:YES completion:nil];
}


-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [self.picController dismissViewControllerAnimated:YES completion:nil];
    self.image = info[UIImagePickerControllerOriginalImage];
    self.imageview.image = self.image;
    [self fadeInImage];
    
    
    
    
}

-(void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(UIImagePickerController *) picController {
    
    if(!_picController) {
        _picController = [[UIImagePickerController alloc] init];
    }
    
    _picController.delegate = self;
    return _picController;
    
}



- (IBAction)add:(id)sender {
    
   
    NSString * prodName = self.productText.text;
    
    NSArray * current = [self.manager getFoodWithName:prodName];
    //if the item added already exists
    // notify the user.
    if(current.count > 0) {
        
        TEFoodItem * f = current[0];
        
        UIAlertView * av = [[UIAlertView alloc] initWithTitle:@"Varan finns redan." message:[NSString stringWithFormat:@"Varan %@ finns redan!",f.name] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [av show];
        
    }
    
    else {
    
        //NSNumber * price = [NSNumber numberWithDouble:self.pricefield.text.doubleValue];
        
        //if the product name is not given return without doing anything
        if(!self.productText.text) {
            return;
        }
        NSNumber * price2 = nil;
        
        if(self.priceField.text) {
        double price = self.priceField.text.doubleValue;
        price2 = [NSNumber numberWithDouble:price];
        
        }
      
        TEFoodItem * newitem = nil;
        
        if(price2) {
        
            newitem = [self.manager createFoodWithName:self.productText.text andPrice:price2];
        }
        
        
        else {
            
            newitem = [self.manager createFoodWithName:self.productText.text];
        }
            
            
        self.productText.text = nil;
        //go back to list
        [self.navigationController popViewControllerAnimated:YES];
        if(self.image) {
            
            newitem.image = self.image;
            [self.manager save];
        }
        
    
    }
}
/* Fades the image in */
-(void) fadeInImage {
    
    self.imageview.alpha = 0;
    
    [UIView beginAnimations:@"fade in" context:nil];
    [UIView setAnimationDuration:3.0];
    self.imageview.alpha = 1.0;
    [UIView commitAnimations];

    }

-(void) textFieldDidBeginEditing:(UITextField *)textField {
 
    [self.removeButton setUserInteractionEnabled:NO];
    [self.addButton setUserInteractionEnabled:NO];
    
}

// The add button should only be enabled if
//food has a name.
- (void)textFieldDidEndEditing:(UITextField *)textField {
    BOOL interaction;
    interaction = (self.productText.text.length > 0);
    
    self.removeButton.userInteractionEnabled = interaction;
    self.addButton.userInteractionEnabled = interaction;
    
    
}



- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}






- (void)viewDidLoad {
    [super viewDidLoad];
    self.manager = [DataManager getInstance];
    [self.addButton setUserInteractionEnabled:NO];
    [self.removeButton setUserInteractionEnabled:NO];
    self.productText.delegate = self;
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

//
//  AddFoodItemController.h
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-07.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
// Add a new fooditem

#import <UIKit/UIKit.h>

@interface AddFoodItemController : UIViewController <UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@end



/* Represents a post on the shoppinglist */
/* a grocery (what to buy) and an amount (how much, how many?)
 */
#import "TEFoodItem.h"
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TE_Buy : NSManagedObject

@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSNumber * amount;
@property (nonatomic, retain) TEFoodItem *grocery;
@end


//
//  TE_Buy.m
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-07.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "TE_Buy.h"
#import "TEFoodItem.h"


@implementation TE_Buy

@dynamic id;
@dynamic amount;
@dynamic grocery;

@end

//
//  TEFoodItem.m
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-01.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "TEFoodItem.h"
#import "TEFoodItem.h"


@implementation TEFoodItem
@dynamic  key;
@dynamic  image;
@dynamic  name;
@dynamic  price;
@dynamic occurences;
-(void) awakeFromInsert {
    
    
    NSUUID * uid = [[NSUUID alloc]init];
    self.key = [uid UUIDString];
}

@end

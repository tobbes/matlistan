//
//  ImageTransformer.m
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-03.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
// This class makes it possible to store images in the coredata database.

#import "ImageTransformer.h"

@implementation ImageTransformer
+ (Class) transformedValueClass {
    
    return [NSData class];
}


-(id) transformedValue:(id)value {
    
    if(!value) {
        return nil;
    }
    
    if([value isKindOfClass:[NSData class]]) {
        return value;
    }
    
    return UIImagePNGRepresentation(value);
}

-(id) reverseTransformedValue:(id)value {
    return [UIImage imageWithData:value];
}
@end



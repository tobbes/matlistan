//
//  TECategory.h
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-01.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
// A category of food (not used).

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>

@interface TECategory : NSManagedObject

@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) UIImage * image;

@end

//
//  AllFoodTable.h
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-08.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
// The list of all available foods

#import <UIKit/UIKit.h>
#import "DataManager.h"
#import "TEShoppingList.h"
#import "TE_Buy.h"
@interface AllFoodTable : UITableViewController <UISearchBarDelegate>
@property (nonatomic) TEShoppingList * currentList;
@property (nonatomic) NSString * listname;
@end

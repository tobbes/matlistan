
//  AllFoodTable.m
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-08.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "AllFoodTable.h"
#import "ViewShoppingList.h"
#import "DataManager.h"
#import "FoodCell.h"
@interface AllFoodTable ()


@property (nonatomic) NSArray * data;
@property (nonatomic) DataManager * manager;
//@property (nonatomic) NSMutableArray * newlist;
//@property (nonatomic) TEShoppingList * shoppinglist;



@end


@implementation AllFoodTable

- (IBAction)plustap:(UIButton *)sender {
    FoodCell * cell =   (FoodCell *) sender.superview.superview;
    [self doUpdateCell:cell increase:YES];
    }




- (IBAction)minustap:(UIButton *)sender {
    FoodCell * cell = (FoodCell *) sender.superview.superview;
    [self doUpdateCell:cell increase:NO];
    }


-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    self.data = [self.manager getFoodMatchingPattern:searchText];
    
    
}


-(void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(editingStyle == UITableViewCellEditingStyleDelete ) {
        
        
        TEFoodItem * f = self.data[indexPath.row];
        
        
        [self.manager deleteFoodWithName:f.name];
        if(tableView != self.tableView) {
            self.data = [self.manager getFoodMatchingPattern:self.searchDisplayController.searchBar.text];
        }
        else {
            
            self.data = [self.manager getFood];
        }
          [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        

    }
}


    - (NSString *) tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
        
 
        return @"Ta bort";
    }
    


-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [self refreshMe];
}

-(void) doUpdateCell:(FoodCell *) cell increase: (BOOL) inc {
    
    int currentValue = cell.amountlabel.text.intValue;
    
    
    if(inc) {
        currentValue++; }
    
    
    else {
        
        if(currentValue > 0) {
            currentValue --;
        }
    }
    
    
    cell.amountlabel.text = [NSString stringWithFormat:@"%d",currentValue];
    
    if(cell.buy != nil) {
    cell.buy.amount = [NSNumber numberWithInt:currentValue];
    [self.manager save];
    }
    
    
    return;
    
}

-(void) viewWillAppear:(BOOL)animated {
    
    [self refreshMe];
}


//called to refresh the datasource when an item has been added or
//removed
-(void) refreshMe {
    self.data = [self.manager getFood];
    [self.tableView reloadData];
}



-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"view_list"]) {
         ViewShoppingList * list = segue.destinationViewController;
        list.list = self.currentList;
    }


}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TEFoodItem * item = self.data[indexPath.row];
    FoodCell * cell = (FoodCell *) [self.tableView cellForRowAtIndexPath:indexPath];
    int amount = cell.amountlabel.text.intValue;
    TE_Buy * buy =  [self.manager createBuyWithItem:item howMany:amount];
    cell.buy = buy;
    [self.currentList addBoughtObject:buy];
    [self.manager save];
    
    
}

-(void) tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TEFoodItem * f = self.data[indexPath.row];
    
    FoodCell * cell = (FoodCell *) [self.tableView cellForRowAtIndexPath:indexPath];
    cell.buy = nil;
    
    
    for (TE_Buy * b in self.currentList.bought) {
        if(b.grocery == f ) {
            [self.currentList removeBoughtObject:b];
        }
    
    [self.manager save];
    }
}






-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
 
    
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(10 , 5, tableView.frame.size.width,10)];
    label.textAlignment = NSTextAlignmentCenter;
    
    label.textColor = [UIColor blueColor];
    label.text = self.listname;
    return label;
}

- (void) viewDidLoad {
    [super viewDidLoad];
    self.tableView.allowsMultipleSelection = YES;
    self.clearsSelectionOnViewWillAppear = NO;
    self.manager = [DataManager getInstance];
    //Create a new shoppinglist with the given name
    self.currentList = [self.manager createShoppingListWithName:self.listname];
    self.title = self.listname;
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return self.data.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView  dequeueReusableCellWithIdentifier:@"foodcell"];
    
    TEFoodItem * f = self.data[indexPath.row];
    
    cell.textLabel.text = f.name;
    if(f.image) {
        cell.imageView.image = f.image;
    }
    
    

 

    FoodCell * fcell = (FoodCell *) cell;
    
    fcell.minusbutton.tag = indexPath.row;
    fcell.plusbutton.tag = indexPath.row;
    return fcell;
}




@end

//
//  DataManager.h
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-01.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
// This is the backend of the system, it takes care of creating, retrieving and deleting
// shoppinglists, items on the shoppinglist etc...

#import "TE_Buy.h"
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "TEFoodItem.h"
#import "TECategory.h"
#import "TEShoppingList.h"
@interface DataManager : NSObject

/* Gets the instance of this singleton */
+(DataManager *) getInstance;

// create a new food without name
- (TEFoodItem *) createFood;
//create a new category (not used currently).
- (TECategory *) createCategory;
/* retrieve all articles */
-(NSArray*) getFood;
/* called to save changes to coredata database */
-(BOOL) save;
/* count the number of different foods */
-(NSUInteger) countFood;

/* Add a new entry to a shoppinglist */
-(TE_Buy *) createBuyWithItem: (TEFoodItem *) item  howMany:(int) amount;
/* Get all entries on all shoppinglists */
-(NSArray *) getBuys;
/* delete food with the given name */
-(BOOL) deleteFoodWithName:(NSString *)name;
/* get the food with the given name */
-(NSArray *) getFoodWithName: (NSString *) name;
/* delete the given shoppinglistentry */
-(void) deleteBuy:(TE_Buy *) buy;
/* create a fooditem with the given name */
-(TEFoodItem *) createFoodWithName:(NSString *) name;
-(TEFoodItem *) createFoodWithName: (NSString *) name andPrice:(NSNumber *) price;
-(TEShoppingList *) getShoppingListWithId:(NSNumber *) id;
-(TEShoppingList *) createShoppingListWithName: (NSString *) name;
-(NSArray *) getShoppingLists;
-(NSArray *) getFoodMatchingPattern: (NSString *) pattern;
-(void) deleteShoppingList:(TEShoppingList *) list;
@end

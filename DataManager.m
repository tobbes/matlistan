//
//  DataManager.m
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-01.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "DataManager.h"
#import "AppDelegate.h"
@interface DataManager ()

@property (nonatomic) NSManagedObjectContext * ctxt;
@property (nonatomic) NSManagedObjectModel * model;

@end


@implementation DataManager


//Gets the path at which database should be stored
-(NSString *) getArchivePath {
    
    
    NSArray * documentDirs;
    documentDirs = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString * directory = documentDirs[0];
    
    return [directory stringByAppendingPathComponent:@"mystore.data"];
    
}


+(DataManager *) getInstance {
    
    static DataManager * manager;
    
    if(!manager) {
        
        //manager = [[DataManager alloc] initPrivate];
        manager = [[DataManager alloc] init2];
    
    }
    
    return manager;
}





-(void) deleteShoppingList:(TEShoppingList *)list {
    [self.ctxt deleteObject:list];
}
- (TEShoppingList *) createShoppingListWithName:(NSString *)name {
    
  TEShoppingList * list =    [NSEntityDescription insertNewObjectForEntityForName:@"TEShoppingList" inManagedObjectContext:self.ctxt];
    
    NSArray * existing = [self getShoppingLists];
    
    NSInteger id = existing.count + 1;
    
    list.id = [NSNumber numberWithLong:id];
    list.name = name;
    [self save];

    
    return list;
    
}


-(TEShoppingList *) getShoppingListWithId:(NSNumber *)id {
    
    TEShoppingList * list;
    NSFetchRequest * req = [[NSFetchRequest alloc] init];
    NSEntityDescription * desc = [NSEntityDescription entityForName:@"TEShoppingList" inManagedObjectContext:self.ctxt];

    
    
    req.entity = desc;
    NSInteger idTouse = id.integerValue;
    
    NSPredicate * pred = [NSPredicate predicateWithFormat:@"id = %d",idTouse];
    
    
    req.predicate = pred;
  
    NSError * error;
    NSArray * res =   [self.ctxt executeFetchRequest:req error:&error];
   
    NSLog(@" Id till getShoppingListwith id: %@",id);
    list = res[0];
   
    return list;
}

- (NSFetchedResultsController *) setupFetchedResultController {
    return nil;
}





-(NSArray *) getShoppingLists {
    NSFetchRequest * req = [[NSFetchRequest alloc] init];
    NSEntityDescription * desc = [NSEntityDescription entityForName:@"TEShoppingList" inManagedObjectContext:self.ctxt];
    
    req.entity = desc;
    
    NSError * error;
    return [self.ctxt executeFetchRequest:req error:&error];
    
    
}


-(void) deleteBuy:(TE_Buy *) buy {
    
    [self.ctxt deleteObject:buy];
    [self save];
}





-(instancetype) init2 {
    
    
    self = [super init];
    
    if(self) {
        
        AppDelegate * delegate = [UIApplication sharedApplication].delegate;
        self.ctxt = delegate.managedObjectContext;
        
    
    }
    
    
    return self;
}



//old code not used...

/*- (instancetype) initPrivate {
    
    
    
    self = [super init];
    
    
    
    NSString * path = [self getArchivePath];
    
    NSURL * storeurl = [NSURL fileURLWithPath:path];
    if(self) {
        
        
        
        _model = [NSManagedObjectModel mergedModelFromBundles:nil];
        
        NSPersistentStoreCoordinator * psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:_model];
        NSError * error;
        if(![psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeurl options:nil error:&error]){
            

            
        
        }
        
        
        _ctxt = [[NSManagedObjectContext alloc] init];
        _ctxt.persistentStoreCoordinator = psc;
        
    }
    return self;


   

} */

-(NSUInteger) countFood {
    NSFetchRequest * req = [[NSFetchRequest alloc] init];
    NSEntityDescription * e = [NSEntityDescription entityForName:@"TEFoodItem" inManagedObjectContext:self.ctxt];
    req.entity = e;
    NSError * error;
    return [self.ctxt countForFetchRequest:req error:&error];
    
    
    
}


-(TEFoodItem *) createFood {
    
    TEFoodItem * item = [NSEntityDescription insertNewObjectForEntityForName:@"TEFoodItem" inManagedObjectContext:self.ctxt];
    [self save];
    return item;
}



-(TEFoodItem *) createFoodWithName: (NSString *) name andPrice:(NSNumber *) price {
    
    TEFoodItem * food = [self createFoodWithName:name];
    food.price = price;
    return food;
    
}


-(BOOL) deleteFoodWithName:(NSString *)name {
 
    NSArray * toDelete = [self getFoodWithName:name];
    
    
    for (TEFoodItem * item in toDelete) {
        [self.ctxt deleteObject:item];
    }
    
    [self save];
    return YES;
}


-(NSArray *) getFoodWithName: (NSString *) name {
    
    
    NSEntityDescription * desc = [NSEntityDescription entityForName:@"TEFoodItem" inManagedObjectContext:self.ctxt];
    NSPredicate * pred = [NSPredicate predicateWithFormat:@"name ==[c] %@",name ];
   
    NSFetchRequest * req = [[NSFetchRequest alloc] init];
    
    req.entity = desc;
    req.predicate = pred;
    NSError * error;
    NSArray * result = [self.ctxt executeFetchRequest:req error:&error];
    
    if(!result) {
        NSLog(@"Fel vid hamtning av matvaror: %@",[error localizedDescription]);
        }
    
    
    return result;
    
}


-(TEFoodItem *) createFoodWithName:(NSString *) name {
    TEFoodItem * f = [self createFood];
    f.name = name;
    [self save];
    return f;
    
}

-(TECategory *) createCategory {
    
    TECategory * cat = [NSEntityDescription insertNewObjectForEntityForName:@"TECategory" inManagedObjectContext:self.ctxt];
    return cat;
}

-(BOOL) save {
    
    NSError * error;
    BOOL sucess = [self.ctxt save:&error];
    
    if(!sucess) {
        NSLog(@"Error saving the model: %@",[error localizedDescription]);
    }
    
    return sucess;
    
}


-(NSArray*) getFood {
    
    NSFetchRequest * req = [[NSFetchRequest alloc] init];
    NSEntityDescription * e = [NSEntityDescription entityForName:@"TEFoodItem"
                                        inManagedObjectContext:self.ctxt];
    
    
    NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
                                 
    
                                 
    
    req.sortDescriptors = @[descriptor];
    req.entity = e;
    [req setReturnsObjectsAsFaults:NO];
    NSError * error;
    NSArray * result = [self.ctxt executeFetchRequest:req error:&error];
    
    if(!result) {
        NSLog(@"Det gick at helvete");
    }
    
    
    for (TEFoodItem * item in result) {
        NSLog(@"%@",item.name);
    }
    return result;
}


-(NSArray *) getFoodMatchingPattern: (NSString *) pattern {
    NSFetchRequest * req = [[NSFetchRequest alloc] init];
    
    NSPredicate * searchpred = [NSPredicate predicateWithFormat:@"name contains[c] %@",pattern];
    NSEntityDescription  * desc = [NSEntityDescription entityForName:@"TEFoodItem" inManagedObjectContext:self.ctxt];
    
    req.entity = desc;
    req.predicate = searchpred;
    
    NSError * error;
    NSArray * result = [self.ctxt executeFetchRequest:req error:&error];
    
    
    
    if(!result) {
        NSLog(@"Det gick åt helvete");
    }
    
    return result;
    
}


-(NSArray *) getBuys {
    
    NSFetchRequest * req = [[NSFetchRequest alloc] init];
    NSEntityDescription * desc = [NSEntityDescription entityForName:@"TE_Buy" inManagedObjectContext:self.ctxt];

    req.entity = desc;
    [req setReturnsObjectsAsFaults:NO];
    
    NSError * error;
    NSArray * result = [self.ctxt executeFetchRequest:req error:&error];

    if(!result) {
        
        NSLog(@"Det gick inte att hämta buys!");
        
    }
    return result;
}

-(TE_Buy *) createBuyWithItem: (TEFoodItem *) item  howMany:(int) amount {
    
    TE_Buy * buy = [NSEntityDescription insertNewObjectForEntityForName:@"TE_Buy" inManagedObjectContext:self.ctxt];
    
    buy.amount = [NSNumber numberWithInt:amount];
    
    buy.grocery = item;
    [self save];
    return buy;
}









@end

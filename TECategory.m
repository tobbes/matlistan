//
//  TECategory.m
//  matlistan
//
//  Created by Tobias Ednersson on 2015-04-01.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "TECategory.h"


@implementation TECategory

@dynamic category;
@dynamic image;

@end
